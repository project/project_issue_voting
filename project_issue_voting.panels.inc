<?php


/**
 * @file
 * Panels integration for the Project issue voting module.
 *
 * At this point, this only provides a default panel page and panel
 * view configuration that are is used for the site-wide issue voting
 * dashboard.
 */

/**
 * Implementation of hook_default_panel_pages.
 */
function _project_issue_voting_default_panel_pages() {
  $pages = array();

  $page = new stdClass();
  $page->pid = 'new';
  $page->name = 'top_issue_votes';
  $page->title = 'Top voted issues';
  $page->arguments = array();
  $page->relationships = array();
  $page->access = array();
  $page->path = 'project/issues/votes/top';
  $page->css_id = '';
  $page->css = '';
  $page->no_blocks = '0';
  $page->switcher_options = array();
  $page->menu = '1';
  $page->menu_tab = '0';
  $page->menu_tab_weight = '0';
  $page->menu_title = 'Top issue votes';
  $page->menu_tab_default = '0';
  $page->menu_tab_default_parent_type = 'tab';
  $page->menu_parent_title = '';
  $page->menu_parent_tab_weight = '0';
  $page->contexts = array();
  $display = new panels_display();
  $display->did = 'new';
  $display->layout = 'onecol';
  $display->layout_settings = array (
    'plain' => 0,
  );
  $display->panel_settings = array (
    'style' => 'default',
    'style_settings' => array(),
    'edit_style' => 'Edit style settings',
    'individual' => 0,
    'panel' => array (
      'middle' => array (
        'style' => '',
        'edit_style' => 'Edit style settings',
      ),
    ),
    'did' => '1',
  );
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1';
  $pane->panel = 'middle';
  $pane->type = 'views2';
  $pane->subtype = 'project_issue_votes_by_category';
  $pane->access = array();
  $pane->configuration = array (
    'override_title' => 0,
    'override_title_text' => '',
    'css_id' => '',
    'css_class' => '',
    'title' => 'Issue votes by category',
    'name' => 'project_issue_votes_by_category',
    'arguments' => array (
      0 => 'feature',
    ),
  );
  $pane->shown = '1';
  $display->content['new-1'] = $pane;
  $display->panels['middle'][0] = 'new-1';

  $pane = new stdClass();
  $pane->pid = 'new-2';
  $pane->panel = 'middle';
  $pane->type = 'views2';
  $pane->subtype = 'project_issue_votes_by_category';
  $pane->access = array();
  $pane->configuration = array (
    'override_title' => 0,
    'override_title_text' => '',
    'css_id' => '',
    'css_class' => '',
    'title' => 'Issue votes by category',
    'name' => 'project_issue_votes_by_category',
    'arguments' => array (
      0 => 'bug',
    ),
  );
  $pane->shown = '1';
  $display->content['new-2'] = $pane;
  $display->panels['middle'][1] = 'new-2';

  $pane = new stdClass();
  $pane->pid = 'new-3';
  $pane->panel = 'middle';
  $pane->type = 'views2';
  $pane->subtype = 'project_issue_votes_by_category';
  $pane->access = array();
  $pane->configuration = array (
    'override_title' => 0,
    'override_title_text' => '',
    'css_id' => '',
    'css_class' => '',
    'title' => 'Issue votes by category',
    'name' => 'project_issue_votes_by_category',
    'arguments' => array (
      0 => 'task',
    ),
  );
  $pane->shown = '1';
  $display->content['new-3'] = $pane;
  $display->panels['middle'][2] = 'new-3';

  $pane = new stdClass();
  $pane->pid = 'new-4';
  $pane->panel = 'middle';
  $pane->type = 'views2';
  $pane->subtype = 'project_issue_votes_by_category';
  $pane->access = array();
  $pane->configuration = array (
    'override_title' => 0,
    'override_title_text' => '',
    'css_id' => '',
    'css_class' => '',
    'title' => 'Issue votes by category',
    'name' => 'project_issue_votes_by_category',
    'arguments' => array (
      0 => 'support',
    ),
  );
  $pane->shown = '1';
  $display->content['new-4'] = $pane;
  $display->panels['middle'][3] = 'new-4';

  $page->display = $display;
  $page->displays = array();
  $pages["$page->name"] = $page;

  return $pages;
}

/**
 * Implementation of hook_default_panel_views.
 */
function _project_issue_voting_default_panel_views() {
  $panel_views = array();

  $panel_view = new stdClass();
  $panel_view->pvid = 'new';
  $panel_view->view = 'project_issue_votes_by_category';
  $panel_view->name = 'project_issue_votes_by_category';
  $panel_view->description = 'View issue votes for a particular category (bug, feature, etc)';
  $panel_view->title = 'Issue votes by category';
  $panel_view->category = 'Views';
  $panel_view->category_weight = '-1';
  // NOTE: Using the block type here prevents the exposed filters and
  // the page header text from being displayed.
  $panel_view->view_type = 'block';
  $panel_view->use_pager = '0';
  $panel_view->pager_id = '0';
  $panel_view->nodes_per_page = '20';
  $panel_view->offset = '0';
  $panel_view->link_to_view = '1';
  $panel_view->more_link = '0';
  $panel_view->more_text = '';
  $panel_view->feed_icons = '0';
  $panel_view->url_override = '0';
  $panel_view->url = '';
  $panel_view->url_from_panel = '0';
  $panel_view->contexts = array (
    0 => array (
      'type' => 'user',
      'context' => 'any',
      'panel' => '0',
      'fixed' => '',
      'label' => 'Project issue: Category',
    ),
  );
  $panel_view->allow_type = NULL;
  $panel_view->allow_nodes_per_page = '1';
  $panel_view->allow_offset = '0';
  $panel_view->allow_use_pager = '0';
  $panel_view->allow_link_to_view = '0';
  $panel_view->allow_more_link = '0';
  $panel_view->allow_more_text = '0';
  $panel_view->allow_feed_icons = '0';
  $panel_view->allow_url_override = '0';
  $panel_view->allow_url_from_panel = '0';

  $panel_views["$panel_view->name"] = $panel_view;

  return $panel_views;
}
