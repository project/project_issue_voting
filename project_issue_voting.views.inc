<?php


/**
 * @file
 * Views integration for the Project issue voting module.
 *
 * At this point, this only provides a default view that is used for
 * the site-wide issue voting dashboard.
 */

/**
 * Implementation of hook_views_default_views.
 */
function _project_issue_voting_views_default_views() {
  $views = array();

  $view = new stdClass();
  $view->name = 'project_issue_votes_by_category';
  $view->description = 'View issue votes for a particular category (bug, feature, etc)';
  $view->access = array (
);
  $view->page = TRUE;
  $view->page_title = '';
  $view->page_header = "<?php print l(t('Site-wide top voted issues'), 'project/issues/votes/top'); ?>";
  $view->page_header_format = '2';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'There are currently no issues to display for the criteria selected.';
  $view->page_empty_format = '1';
  $view->page_type = 'project_issues';
  $view->url = 'project/issues/votes/category';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '50';
  $view->menu = TRUE;
  $view->menu_title = 'Issues by category';
  $view->menu_tab = FALSE;
  $view->menu_tab_weight = '0';
  $view->menu_tab_default = FALSE;
  $view->menu_tab_default_parent = NULL;
  $view->menu_tab_default_parent_type = 'tab';
  $view->menu_parent_tab_weight = '0';
  $view->menu_parent_title = '';
  $view->block = TRUE;
  $view->block_title = '';
  $view->block_header = '';
  $view->block_header_format = '1';
  $view->block_footer = '';
  $view->block_footer_format = '1';
  $view->block_empty = '';
  $view->block_empty_format = '1';
  $view->block_type = 'project_issues';
  $view->nodes_per_block = '20';
  $view->block_more = FALSE;
  $view->block_use_page_header = FALSE;
  $view->block_use_page_footer = FALSE;
  $view->block_use_page_empty = TRUE;
  $view->sort = array (
  );
  $view->argument = array (
    array (
      'type' => 'project_issue_category',
      'argdefault' => '4',
      'title' => 'Top voted %1',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'votingapi_cache_issue_points_sum',
      'field' => 'value',
      'label' => 'Votes',
      'handler' => 'votingapi_views_formatter_raw',
      'sortable' => '1',
      'defaultsort' => 'DESC',
    ),
    array (
      'tablename' => 'project_issue_project_node',
      'field' => 'title',
      'label' => 'Project',
      'handler' => 'views_handler_field_project_issue_project_nodelink',
      'sortable' => '1',
      'options' => 'link',
    ),
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Summary',
      'handler' => 'views_handler_field_nodelink_with_mark',
      'sortable' => '1',
      'options' => 'link',
    ),
    array (
      'tablename' => 'project_issues',
      'field' => 'sid',
      'label' => 'Status',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'project_issues',
      'field' => 'priority',
      'label' => 'Priority',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'project_issues',
      'field' => 'rid',
      'label' => 'Version',
      'sortable' => '1',
      'options' => 'project_issue_rid_allow_hide',
    ),
    array (
      'tablename' => 'node',
      'field' => 'changed',
      'label' => 'Last updated',
      'handler' => 'views_handler_field_since',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'pi_users',
      'field' => 'name',
      'label' => 'Assigned to',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'project_issue_project_projects',
      'field' => 'uri',
      'label' => 'Short name',
      'options' => 'project_issue_nodisplay',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'project_issues',
      'field' => 'sid',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'defaults',
),
    ),
    array (
      'tablename' => 'project_issue_project_node',
      'field' => 'nid',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
),
    ),
    array (
      'tablename' => 'project_issues',
      'field' => 'priority',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
),
    ),
    array (
      'tablename' => 'votingapi_cache_issue_points_sum',
      'field' => 'value',
      'operator' => '>',
      'options' => '',
      'value' => '0',
    ),
  );
  $view->exposed_filter = array (
    array (
      'tablename' => 'project_issue_project_node',
      'field' => 'nid',
      'label' => 'Project',
      'optional' => '1',
      'is_default' => '0',
      'operator' => '1',
      'single' => '1',
    ),
    array (
      'tablename' => 'project_issues',
      'field' => 'sid',
      'label' => 'Status',
      'optional' => '1',
      'is_default' => '1',
      'operator' => '1',
      'single' => '1',
    ),
    array (
      'tablename' => 'project_issues',
      'field' => 'priority',
      'label' => 'Priority',
      'optional' => '1',
      'is_default' => '0',
      'operator' => '1',
      'single' => '1',
    ),
  );
  $view->requires = array(votingapi_cache_issue_points_sum, project_issue_project_node, node, project_issues, pi_users, project_issue_project_projects);
  $views[$view->name] = $view;

  return $views;
}

