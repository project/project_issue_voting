
The Project issue voting module allows users to vote on project issues
via a JavaScript degradable voting widget.  Administrators can
restrict voting by project type, issue category, and issue status.
They can also configure the total number of votes allowed per user.

For complete installation instructions, read the INSTALL.txt file.

This module depends on:
Project issue tracking (http://drupal.org/project/project_issue)
Voting API (http://drupal.org/project/votingapi)

If you install Views (http://drupal.org/project/views) and Panels2
(http://drupal.org/project/panels), the module exports some default
views and panel pages that provide a dashboard of issue voting on the
site.

Send feature requests and bug reports to the issue tracking system:
http://drupal.org/node/add/project-issue/project_issue_voting

This module was co-developed by:
- Chad Phillips (http://drupal.org/user/22079/) a.k.a. "hunmonk"
- Derek Wright (http://drupal.org/user/46549) a.k.a. "dww"

Loosely based on the "Vote Up/Down" package:
http://drupal.org/project/vote_up_down

Originally sponsored by Acquia: http://acquia.com
