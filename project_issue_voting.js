
Drupal.projectIssueVotingAutoAttach = function() {
  var vdb = [];
  $('span.project-issue-voting-vote-up').each(function() {
    // Remove href link
    $(this).html('');
    var nid = $(this).attr('nid');
    $(this).click(function() {
      Drupal.projectIssueVotingVote(nid, '1');
    });
  });
  $('span.project-issue-voting-vote-down').each(function() {
    // Remove href link.
    $(this).html('');
    var nid = $(this).attr('nid');
    $(this).click(function() {
      Drupal.projectIssueVotingVote(nid, '-1');
    });
  });
}

Drupal.projectIssueVotingVote = function(nid, vote) {
  var token = Drupal.settings.projectIssueVotingToken;
  var url = Drupal.settings.projectIssueVotingURL + "/" + nid + "/" + vote + "/" + token + "/1";
  // Ajax GET request for vote data.
  $.ajax({
    type: "GET",
    url: url,
    dataType: "json",
    success: function(data) {
      // Update the points.
      $('#project-issue-voting-votes-total-wrapper-' + nid).html(data.total);
      $('#project-issue-voting-votes-' + nid).html(data.user);

      if ($('#project-issue-voting-total-votes-link-' + nid).size() > 0) {
        $('#project-issue-voting-total-votes-link-' + nid).html(data.total_votes_link_votes);
      }

      if ($('#project-issue-voting-metadata-table-total-votes-' + nid).size() > 0) {
        $('#project-issue-voting-metadata-table-total-votes-' + nid).html(data.total_votes_metadata_table);
      }

      // Update the add vote indicator.
      if (data.vote_add == true) {
        $('#project-issue-voting-vote-up-' + nid)
          .removeClass('project-issue-voting-vote-up-inact')
          .addClass('project-issue-voting-vote-up-act');
      }
      else {
        $('#project-issue-voting-vote-up-' + nid)
          .removeClass('project-issue-voting-vote-up-act')
          .addClass('project-issue-voting-vote-up-inact');
      }

      // Update the subtract vote indicator.
      if (data.vote_subtract == true) {
        $('#project-issue-voting-vote-down-' + nid)
          .removeClass('project-issue-voting-vote-down-inact')
          .addClass('project-issue-voting-vote-down-act');
      }
      else {
        $('#project-issue-voting-vote-down-' + nid)
          .removeClass('project-issue-voting-vote-down-act')
          .addClass('project-issue-voting-vote-down-inact');
      }

    },
    error: function(xmlhttp) {
      alert('An HTTP error '+ xmlhttp.status +' occured.\n'+ db.uri);
    }
  });
}

// Global killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.projectIssueVotingAutoAttach);
}
